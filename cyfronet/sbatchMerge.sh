#!/bin/bash
if [ "$#" -ne 1 ]; then
  echo "Invalid invocation."
  exit 1
fi

sbatch \
  -N 1 \
  --cpus-per-task=1 \
  --mem=20GB \
  -p plgrid \
  --time=00:59:00 \
  -J "mer$1" \
  -A dyskrepancje2 \
  --output="$PLG_USER_STORAGE/output/mer$1" \
  --error="$PLG_USER_STORAGE/error/mer$1" \
  runMerge.sh $1
