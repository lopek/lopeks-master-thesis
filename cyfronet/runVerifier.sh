#!/bin/bash -l

module load plgrid/tools/gcc/7.1.0

cd $PLG_USER_STORAGE/lopeks-master-thesis
g++ verifier.cpp -o $TMPDIR/verifier -std=c++1y  -pthread -O3 -Wall

cd $SCRATCH
time $TMPDIR/verifier 2000880 499800000 /net/archive/groups/plggerdos/m$1 <<< "$2 $3 $4 $5"
