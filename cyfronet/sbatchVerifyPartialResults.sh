#!/bin/bash
if [ "$#" -ne 1 ]; then
  echo "Invalid invocation."
  exit 1
fi

SUF=`echo $1 | rev | cut -d/ -f1 | rev`

sbatch \
  -N 1 \
  --cpus-per-task=1 \
  --mem=10GB \
  -p plgrid \
  --time=04:59:00 \
  -J "verR_$SUF" \
  -A dyskrepancje2 \
  --output="$PLG_USER_STORAGE/output/verifyPartialCounts/stdout_$SUF" \
  --error="$PLG_USER_STORAGE/error/verifyPartialCounts/stderr_$SUF" \
  runVerifyPartialCounts.sh $1
