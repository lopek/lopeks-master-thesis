#!/bin/bash
if [ "$#" -ne 5 ]; then
  echo "Invalid invocation (000 -1 1 -1 1) is good."
  exit 1
fi

sbatch \
  -N 1 \
  --cpus-per-task=24 \
  --mem=20GB \
  -p plgrid \
  --time=03:59:00 \
  -J "v_$1" \
  -A dyskrepancje2 \
  --output="$PLG_USER_STORAGE/output/verifier/stdout_$1" \
  --error="$PLG_USER_STORAGE/error/verifier/stderr_$1" \
  runVerifier.sh $1 $2 $3 $4 $5
