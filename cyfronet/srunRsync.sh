#!/bin/bash

srun \
  -N 1 \
  --cpus-per-task=1 \
  --mem=20GB \
  -p plgrid \
  --time=00:59:00 \
  -A dyskrepancje2 \
  rsync -r -v --ignore-existing /net/scratch/people/plglopek/counts/ /net/archive/groups/plggerdos/counts
