#!/bin/bash -l

module load plgrid/tools/gcc/7.1.0

cd $PLG_USER_STORAGE/lopeks-master-thesis
g++ collectCounts.cpp -o $TMPDIR/collectCounts -std=c++1y  -pthread -O3 -Wall

cd $SCRATCH
time $TMPDIR/collectCounts 3332 $1 $2 slices/ counts/
