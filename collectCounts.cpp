#include "lib/countcollector.h"
#include "lib/constants.h"
#include "lib/sliceloader.h"

using namespace std;

int main(int argc, const char* argv[]) {
  if (argc != 6) {
    printf("Usage: %s total_slices start_slice stop_slice slices_dir counts_dir\n", argv[0]);
    return 1;
  }
  
  int totalSlices = atoi(argv[1]);
  int startSlice = atoi(argv[2]);
  int endSlice = atoi(argv[3]);
  assert(0 <= startSlice);
  assert(startSlice <= endSlice);
  assert(endSlice < totalSlices);

  string slicesDir = argv[4];
  string countsDir = argv[5];

  auto sliceLoader = make_shared<SliceLoader>(slicesDir);
  for (int sliceId = startSlice; sliceId <= endSlice; sliceId++) {
    assert(sliceLoader->sliceExists(sliceId));
  }

  auto primes = make_shared<Primes>(SLICE_SIZE * totalSlices);
  vector<shared_ptr<CountCollector>> countCollectors;
  vector<string> subdirs;

  CountCollector::init(totalSlices);
  for (auto f : FUNCTIONS) {
    bool s2 = get<0>(f);
    bool s3 = get<1>(f);
    bool s5 = get<2>(f);
    bool s7 = get<3>(f);
    countCollectors.push_back(
        make_shared<CountCollector>(primes->getPrimes(), s2, s3, s5, s7));
    subdirs.push_back(
        toString(s2) + toString(s3) + toString(s5) + toString(s7));
  }

  for (int sliceId = startSlice; sliceId <= endSlice; sliceId++) {
    int64_t* slice = sliceLoader->loadSlice(sliceId);
    for (auto countCollector : countCollectors) {
      countCollector->collectCounts(sliceId, slice);
    }
  }

  for (size_t i = 0; i < FUNCTIONS.size(); i++) {
    auto countCollector = countCollectors[i];
    auto subdir = subdirs[i];

    auto countsFile = make_shared<BinaryFile>(
        countsDir + "/" + subdir + "/r_" + argv[2] + "_" + argv[3]);
    countCollector->storeCounts(countsFile);
  }
  CountCollector::destruct();
  return 0;
}
