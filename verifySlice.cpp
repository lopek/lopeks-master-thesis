#include "lib/computelambda.h"
#include "lib/sliceloader.h"

#include <cstdio>

using namespace std;

const int64_t totalChecks = 1129211;

int main(int argc, const char* argv[]) {
  if (argc != 3) {
    printf("Usage: %s slice_id slices_dir\n", argv[0]);
    return 1;
  }

  int sliceId = atoi(argv[1]);
  string slicesDir = argv[2];
  auto sliceLoader = make_shared<SliceLoader>(slicesDir);
  int64_t* slice = sliceLoader->loadSlice(sliceId);
  int64_t sliceStart = SLICE_SIZE * sliceId + 1;
  int64_t mismatches = 0;
  for (int64_t d = 0; d < totalChecks; d++) {
    int64_t n = sliceStart + ((SLICE_SIZE - 1) * d) / totalChecks;
    while (n % 2 == 0 || n % 3 == 0 || n % 5 == 0 || n % 7 == 0) n++;
    int64_t nidx = INDEX(n - sliceStart + 1); 
    int64_t k = nidx / 64;
    int64_t j = nidx % 64;
    bool nval = (slice[k] & (((uint64_t) 1) << j));
    bool actual = lambda(n);
    if (actual != nval) {
      mismatches++;
      printf(
        "Mismatch found for n = %ld: got %d, expected %d\n", n, nval, actual);
      printf("RINDEX: %ld\n", RINDEX(k, j));
      fflush(stdout);
    }
  }
  printf("Found %ld mismatches in %ld checks.\n", mismatches, totalChecks);
  return 0;
}
