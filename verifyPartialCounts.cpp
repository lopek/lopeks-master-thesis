#include "lib/binaryfile.h"
#include "lib/computelambda.h"
#include "lib/constants.h"

#include <set>

using namespace std;

struct Params {
  int start, end;
  bool s2, s3, s5, s7;
};

struct Data {
  const int64_t totalSlices;
  const int64_t samplesCount;
  const int64_t samplesInSlice;
  const int64_t colSampleStart, colSampleEnd;
  const int64_t colNumStart, colNumEnd;
  const int32_t* samples;

  Data(const Params& params, const string& fileName, int64_t totalSlices) :
      totalSlices(totalSlices),
      samplesCount(SLICE_SIZE * totalSlices / SAMPLE_INTERVAL),
      samplesInSlice(SLICE_SIZE / SAMPLE_INTERVAL),
      colSampleStart(params.start * samplesInSlice),
      colSampleEnd((params.end + 1) * samplesInSlice - 1),
      colNumStart(colSampleStart * SAMPLE_INTERVAL + 1),
      colNumEnd((colSampleEnd + 1) * SAMPLE_INTERVAL),
      samples(new int32_t[samplesCount]) {

    BinaryFile file(fileName);
    file.read((char*) samples, samplesCount * 4);
  }

  ~Data() {
    delete[] samples;
  }
};

Params parseFileName(string fileName) {
  Params params;

  auto idx = fileName.find("counts/");
  if (idx == string::npos) {
    throw runtime_error("File does't contain 'counts/' substring");
  }

  string suf = fileName.substr(idx + 7).c_str();
  assert(suf[0] == '0' || suf[0] == '1');
  assert(suf[1] == '0' || suf[1] == '1');
  assert(suf[2] == '0' || suf[2] == '1');
  assert(suf[3] == '0' || suf[3] == '1');
  params.s2 = (suf[0] == '0' ? 0 : 1);
  params.s3 = (suf[1] == '0' ? 0 : 1);
  params.s5 = (suf[2] == '0' ? 0 : 1);
  params.s7 = (suf[3] == '0' ? 0 : 1);

  assert(suf.substr(4, 3) == "/r_");
  suf = suf.substr(7);
  idx = suf.find("_");
  params.start = atoi(suf.substr(0, idx).c_str());
  params.end = atoi(suf.substr(idx + 1).c_str());
  assert(fileName.find("_" + toString(params.start)) != string::npos);
  assert(fileName.find("_" + toString(params.end)) != string::npos);
  assert(params.start <= params.end);
  return params;
}

void verifyNotAffectedSamples(const Data& data) {
  for (int64_t i = 0; i < data.colSampleStart; i++) {
    assert(data.samples[i] == 0);
  }
  assert(data.samples[data.colSampleStart] != 0);
}

void verifySample(const Params& params, const Data& data, int64_t sampleIdx) {
  assert(data.colSampleStart <= sampleIdx);
  assert(sampleIdx < data.samplesCount);

  int64_t numStart = sampleIdx * SAMPLE_INTERVAL + 1;
  int64_t numEnd = numStart + SAMPLE_INTERVAL - 1;
  int32_t sum = 0;
  for (int64_t N = numStart; N <= numEnd; N++) {
    int64_t n = N;
    bool res = 1;
    while (n % 2 == 0) { n /= 2; if (!params.s2) res = !res; }
    while (n % 3 == 0) { n /= 3; if (!params.s3) res = !res; }
    while (n % 5 == 0) { n /= 5; if (!params.s5) res = !res; }
    while (n % 7 == 0) { n /= 7; if (!params.s7) res = !res; }
    if (n < data.colNumStart || data.colNumEnd < n) {
      continue;
    }
    res = (res == lambda(n));
    if (res) sum++;
  }
  printf(
      "Sample %ld: actual = %d, collected = %d\n",
      sampleIdx,
      sum,
      data.samples[sampleIdx]);
  fflush(stdout);
  assert(sum == data.samples[sampleIdx]);
}

int main(int argc, const char* argv[]) {
  if (argc != 3 || atoi(argv[1]) <= 0) {
    printf("Usage: %s total_slices path_to_counts_file \n", argv[0]);
    return 1;
  }

  int totalSlices = atoi(argv[1]);
  string fileName(argv[2]);

  Params params = parseFileName(fileName);
  printf("Signs: %d %d %d %d\n", params.s2, params.s3, params. s5, params.s7);
  printf("Start slice: %d, end slice: %d\n", params.start, params.end);

  Data data(params, fileName, totalSlices);
  verifyNotAffectedSamples(data);

  set<int> samplesToVerify;
  for (int d = 0; d <= 17; d++) {
    int sampleIdx =
      data.colSampleStart + (data.colSampleEnd - data.colSampleStart) * d / 17;
    samplesToVerify.insert(sampleIdx);
  }
  for (int d = 0; d <= 23; d++) {
    int sampleIdx =
      data.colSampleEnd + 1 + (data.samplesCount - 1 - (data.colSampleEnd + 1)) * d / 23;
    samplesToVerify.insert(sampleIdx);
  }
  for (int sampleIdx : samplesToVerify) {
    verifySample(params, data, sampleIdx);
  }
  printf("You're all good :)\n");
  return 0;
}
