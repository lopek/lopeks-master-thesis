#include "lib/binaryfile.h"
#include "lib/constants.h"

int32_t* samples;
int32_t* aggSamples;

int main(int argc, const char* argv[]) {
  int totalSlices = atoi(argv[1]);
  if (totalSlices <= 0 || argc < 5) {
    printf("Usage: %s total_slices merged_counts_output_file counts_file_1 ... counts_file_n\n", argv[0]);
    return 1;
  }

  const int64_t samplesCount = SLICE_SIZE * totalSlices / SAMPLE_INTERVAL;
  samples = new int32_t[samplesCount];
  aggSamples = new int32_t[samplesCount];

  for (int i = 3; i < argc; i++) {
    BinaryFile file(argv[i]);
    if (i == 3) {
      file.read((char*) aggSamples, samplesCount * 4);
      continue;
    }
    file.read((char*) samples, samplesCount * 4);
    for (int64_t i = 0; i < samplesCount; i++) {
      aggSamples[i] += samples[i];
    }
  }

  BinaryFile mergedCountsFile(argv[2]);
  mergedCountsFile.truncate();
  mergedCountsFile.write((char*) &aggSamples[0], samplesCount * 4);

  delete[] samples;
  delete[] aggSamples;
  return 0;
}
