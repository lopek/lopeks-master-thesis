#include "lib/binaryfile.h"
#include "lib/computelambda.h"

#include <algorithm>
#include <atomic>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <thread>
#include <vector>
#include <sstream>
using namespace std;

#define NUMBER_OF_THREADS 24
#define CHUNK_SIZE ((int64_t) 500)
#define REGULAR_CHECKPOINTS 31
#define RANDOM_CHECKPOINTS 100

vector<int64_t> positivePrimes;

void checkInterval(int64_t start, int64_t end, int64_t sum) {
  atomic<int64_t> nextChunk(start);
  atomic<int64_t> vSum(0);

  auto worker = [&]() {
    while(true) {
      int64_t mStart = nextChunk.fetch_add(CHUNK_SIZE);
      if (mStart > end) {
        break;
      }

      int64_t mSum = 0;
      int64_t mLen = min(CHUNK_SIZE, end - mStart + 1);
      for (int64_t n = mStart; n < mStart + mLen; n++) {
        auto m = n;
        for (auto p : positivePrimes) {
          while (m % p == 0) {
            m /= p;
          }
        }

        if (lambda(m)) {
          mSum++;
        }
      }
      vSum += mSum - (mLen- mSum);
    }
  };

  vector<thread> workerThreads;
  for (int i = 0; i < NUMBER_OF_THREADS; i++) {
    workerThreads.push_back(thread(worker));
  }

  for (auto& workerThread : workerThreads) {
    workerThread.join();
  }

  if (vSum != sum) {
    cout << "Sum from " << start << " to " << end << " is " << vSum
         << " not " << sum << "!" << endl;
    assert(false);
  } else {
    cout << "Sum from " << start << " to " << end << " is indeed " << sum << "."
         << endl;
  }
}

void checkCounts(
    const int32_t* samples, int64_t sampleInterval, int64_t totalSamples) {
  int64_t N = totalSamples - 1;

  vector<int64_t> checkpoints = {
    0, 1,
    N / 2 - 3, N / 2 - 2, N / 2 - 1, N / 2, N / 2 + 1, N / 2 + 2, N / 2 + 3,
    N / 3 - 2, N / 3 - 1, N / 3, N / 3 + 1, N / 3 + 2,
    2 * N / 3 - 2, 2 * N / 3 - 1, 2 * N / 3, 2 * N / 3 + 1, 2 * N / 3 + 2,
    N - 1, N
  };


  for (int64_t c = 1; c <= REGULAR_CHECKPOINTS; c++) {
    checkpoints.push_back(c * N / (REGULAR_CHECKPOINTS + 1));
  }

  for (int64_t c = 1; c <= RANDOM_CHECKPOINTS; c++) {
    checkpoints.push_back(rand() % totalSamples);
  }

  sort(checkpoints.begin(), checkpoints.end());
  checkpoints.resize(
      unique(checkpoints.begin(), checkpoints.end()) - checkpoints.begin());
  assert(checkpoints[0] == 0);
  assert(checkpoints.back() == N);

  for (auto c : checkpoints) {
    checkInterval(
        sampleInterval * c + 1,
        sampleInterval * (c + 1),
        samples[c] - (sampleInterval - samples[c]));
  }
}

int main(int argc, const char* argv[]) {
  srand(time(0));
  if (argc != 4 || atoi(argv[1]) <= 0 || atoi(argv[2]) <= 0) {
    printf("Usage: %s sample_interval total_samples merged_counts_file\n", argv[0]);
    return 1;
  }

  const int64_t sampleInterval = atoi(argv[1]);
  const int64_t totalSamples = atoi(argv[2]);
  string fileName(argv[3]);
  assert(sampleInterval % 840 == 0);

  cout << "SIGNS: " << std::flush;
  int s2, s3, s5, s7;
  cin >> s2 >> s3 >> s5 >> s7;
  assert(s2 * s3 * s5 * s7 * s2 * s3 * s5 * s7 == 1);
  if (s2 == 1) positivePrimes.push_back(2);
  if (s3 == 1) positivePrimes.push_back(3);
  if (s5 == 1) positivePrimes.push_back(5);
  if (s7 == 1) positivePrimes.push_back(7);
  cout << endl << "Positive primes: ";
  for (auto p : positivePrimes) {
    cout << p << " ";
  }
  cout << endl;

  const int32_t* samples = new int32_t[totalSamples];
  BinaryFile file(fileName);
  file.read((char*) samples, totalSamples * 4);

  checkCounts(samples, sampleInterval, totalSamples);
  return 0;
}
