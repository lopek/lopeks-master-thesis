#pragma once

#include "binaryfile.h"
#include "common.h"

#include <cstring>
#include <fstream>
#include <map>
#include <sstream>

class SliceLoader {
 private:
  std::string dir_;
  int64_t* slice_;
  std::map<int, int64_t*> newSlices_;
  int maxSliceAvailable_;

 public:
  SliceLoader(std::string dir) : dir_(dir), maxSliceAvailable_(-1) {
    if (dir_.back() != '/') {
      dir_ += '/';
    }

    slice_ = new int64_t[WORDS_IN_SLICE];

    while (sliceExists(maxSliceAvailable_ + 1)) {
      maxSliceAvailable_++;
    }
    printf("# Max available slice is %d.\n", maxSliceAvailable_);
  }

  ~SliceLoader() {
    delete slice_;
    for (auto buf : newSlices_) {
      delete buf.second;
    }
  }

  bool sliceExists(int sliceId) {
    return BinaryFile(fileForSlice(sliceId)).exists();
  }

  int getMaxSliceAvailable() {
    return maxSliceAvailable_;
  }

  int64_t* loadSlice(int sliceId) {
    BinaryFile sliceFile(fileForSlice(sliceId));
    sliceFile.read(
        (char*) &(slice_[0]),
        WORDS_IN_SLICE * (sizeof(int64_t) / sizeof(char)));
    return slice_;
  }

  int64_t* allocSlice(int otherId) {
    assert(otherId > maxSliceAvailable_);
    newSlices_[otherId] = new int64_t[WORDS_IN_SLICE];
    std::memset(newSlices_[otherId], 0, WORDS_IN_SLICE * (sizeof(int64_t) / sizeof(char)));
    return newSlices_[otherId];
  }

  void storeSlicesOnDisk() {
    for (auto buf : newSlices_) {
      int newSliceId = buf.first;
      int64_t* newSlice = buf.second;
      BinaryFile newSliceFile(fileForSlice(newSliceId));
      newSliceFile.write(
          (char*) &newSlice[0],
          WORDS_IN_SLICE * (sizeof(int64_t) / sizeof(char)));
    }
  }

 private:
  std::string fileForSlice(int sliceId) {
    std::stringstream fileNameSs;
    fileNameSs << dir_ << sliceId;
    return fileNameSs.str();
  }
};
