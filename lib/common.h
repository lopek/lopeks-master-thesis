#pragma once

#include "constants.h"

#include <chrono>
#include <cmath>
#include <cstdarg>
#include <functional>
#include <mutex>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

void runMultiThreaded(
    std::function<void()> worker,
    int64_t threadsCount = NUMBER_OF_THREADS) {
  std::vector<std::thread> workerThreads;
  for (int i = 0; i < threadsCount; i++) {
    workerThreads.push_back(std::thread(worker));
  }

  for (auto& workerThread : workerThreads) {
    workerThread.join();
  }
}

template <typename T>
std::string toString(const T& arg) {
  std::stringstream ss;
  ss << arg;
  return ss.str();
}

class Printf {
 private:
  static std::mutex printfMutex_;

 public:
  Printf() {
    printfMutex_.lock();
  }

  void printf(std::string fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vprintf(fmt.c_str(), args);
    va_end(args);
  }

  void flush() {
    fflush(stdout);
  }

  ~Printf() {
    printfMutex_.unlock();
  }
};
std::mutex Printf::printfMutex_;

class Stopwatch {
 private:
  std::chrono::time_point<std::chrono::system_clock> start_;
  std::string msg_;
  bool shouldFlush_;

 public:
  Stopwatch(std::string msg, bool shouldFlush = false)
    : start_(std::chrono::system_clock::now()),
      msg_(msg),
      shouldFlush_(shouldFlush) {}

  ~Stopwatch() {
    std::chrono::duration<double> d = std::chrono::system_clock::now() - start_;
    Printf ptf;
    ptf.printf("# %s took %lf seconds.\n", msg_.c_str(), d.count());
    if (shouldFlush_) {
      ptf.flush();
    }
  }
};

class Primes {
 private:
   std::vector<int32_t> primes_;

 public:
  Primes(int64_t range) {
    Stopwatch stopwatch("Computing primes");
    const int64_t maxPrime = sqrtl(range) + 1;
    std::vector<bool> isPrime(maxPrime + 1, true);
    for (int64_t k = 2; k <= maxPrime; k++) {
      if (isPrime[k]) {
        primes_.push_back(k);
        for (int64_t l = k * k; l <= maxPrime; l += k) {
          isPrime[l] = false;
        }
      }
    }
    primes_.push_back(1000000007);
  }

  const std::vector<int32_t>& getPrimes() {
    return primes_;
  }
};

