#include <cstdlib>

int64_t powerModPrime(__int128_t n, int64_t k, int64_t p){
  __int128_t r = 1;
  while (k > 0) {
    if (k % 2 == 1) {
      r = r * n % p;
    }
    n = n * n % p;
    k /= 2;
  }
  return r;
}

bool millerRabinSingleTest(int64_t p, int64_t d, __int128_t a) {
  a %= p;
  if (!a) {
    return true;
  } else if (powerModPrime(a, p - 1, p) != 1) {
    return false;
  }
  
  a = powerModPrime(a, d, p);
  if (a == 1) {
    return true;
  }

  while (a != 1) {
    if (a == p - 1) {
      return true;
    }
    a = a * a % p;
  }
  return false;
}

bool millerRabinTest(int64_t n) {
  if (n % 2 == 0) {
    return false;
  }

  int64_t d = n - 1;
  while (d % 2 == 0) {
    d /= 2;
  }
  return    millerRabinSingleTest(n, d, 2)
         && millerRabinSingleTest(n, d, 325)
         && millerRabinSingleTest(n, d, 9375)
         && millerRabinSingleTest(n, d, 28178)
         && millerRabinSingleTest(n, d, 450775)
         && millerRabinSingleTest(n, d, 9780504)
         && millerRabinSingleTest(n, d, 1795265022);
}

#define factorout(d) { while (n % d == 0) { res = !res; n /= d; } }
#define test(dexp) { int64_t d = (dexp); int64_t c = n / d; if (c * d == n) { do { res = !res; n /= d; } while (n % d == 0); if (n == 1) { return res; } if (millerRabinTest(n)) { return !res; } } }

bool lambda(int64_t n) {
  bool res = true;
  factorout(2);
  factorout(3);
  factorout(5);
  factorout(7);

  if (n == 1) {
    return res;
  }

  if (millerRabinTest(n)) {
    return !res;
  }

  int64_t N = n;
  for (int64_t x = 11; x * x <= N; x += 210) {
    test(x); test(x + 2); test(x + 6); test(x + 8); test(x + 12);
    test(x + 18); test(x + 20); test(x + 26); test(x + 30); test(x + 32);
    test(x + 36); test(x + 42); test(x + 48); test(x + 50); test(x + 56);
    test(x + 60); test(x + 62); test(x + 68); test(x + 72); test(x + 78);
    test(x + 86); test(x + 90); test(x + 92); test(x + 96); test(x + 98);
    test(x + 102); test(x + 110); test(x + 116); test(x + 120); test(x + 126);
    test(x + 128); test(x + 132); test(x + 138); test(x + 140); test(x + 146);
    test(x + 152); test(x + 156); test(x + 158); test(x + 162); test(x + 168);
    test(x + 170); test(x + 176); test(x + 180); test(x + 182); test(x + 186);
    test(x + 188); test(x + 198); test(x + 200);
  }
  return !res;
}
