#pragma once

#include <memory>
#include <tuple>
#include <vector>

#define CYFRONET 1

#ifdef LAPTOP
const int64_t SAMPLE_INTERVAL = 840 * 2 * 596; // 1001280
const int64_t CHUNK_SIZE = SAMPLE_INTERVAL * 10;
const int64_t SLICE_SIZE = CHUNK_SIZE * 100; // 27 MB
const int64_t NUMBER_OF_THREADS = 4;

const std::vector<std::tuple<bool, bool, bool, bool>> FUNCTIONS = {
  std::make_tuple(false, false, false, false), // L_P
  std::make_tuple(true, false, false, false),  // L_P \ {2}
  std::make_tuple(true, true, true, false),    // L_P \ {2, 3, 5}
};

#endif

#ifdef STUDENT
const int64_t SAMPLE_INTERVAL = 840 * 2 * 596; // 1001280
const int64_t CHUNK_SIZE = SAMPLE_INTERVAL * 10;
const int64_t SLICE_SIZE = CHUNK_SIZE * 1000; // 270 MB
const int64_t NUMBER_OF_THREADS = 44;

const std::vector<std::tuple<bool, bool, bool, bool>> FUNCTIONS = {
  std::make_tuple(false, false, false, false), // L_P
  std::make_tuple(true, false, false, false),  // L_P \ {2}
  std::make_tuple(false, true, false, false),  // L_P \ {3}
  std::make_tuple(false, false, true, false),  // L_P \ {5}
  std::make_tuple(false, false, false, true),  // L_P \ {7}
  std::make_tuple(true, true, false, false),   // L_P \ {2, 3}
  std::make_tuple(true, true, true, false),    // L_P \ {2, 3, 5}
  std::make_tuple(true, true, true, true)      // L_P \ {2, 3, 5, 7}
};

#endif

#ifdef CYFRONET
// Total slices: 3332
// On disk slices: 303
const int64_t SAMPLE_INTERVAL = 840 * 2 * 1191; // 2000880
const int64_t CHUNK_SIZE = SAMPLE_INTERVAL * 5;
const int64_t SLICE_SIZE = CHUNK_SIZE * 30000; // 8.1 GB
const int64_t NUMBER_OF_THREADS = 24;

const std::vector<std::tuple<bool, bool, bool, bool>> FUNCTIONS = {
  std::make_tuple(false, false, false, false), // L_P
  std::make_tuple(true, false, false, false),  // L_P \ {2}
  std::make_tuple(false, true, false, false),  // L_P \ {3}
  std::make_tuple(false, false, true, false),  // L_P \ {5}
  std::make_tuple(false, false, false, true),  // L_P \ {7}
  std::make_tuple(true, true, false, false),   // L_P \ {2, 3}
  std::make_tuple(true, true, true, false),    // L_P \ {2, 3, 5}
  std::make_tuple(true, true, true, true)      // L_P \ {2, 3, 5, 7}
};

#endif

const int64_t SEGMENT_LEN = 840;
const int64_t VALS_IN_SEGMENT = 192;
const int64_t WORDS_IN_SEGMENT = VALS_IN_SEGMENT / 64;
const int64_t WORDS_IN_SAMPLE_INTERVAL = (SAMPLE_INTERVAL / SEGMENT_LEN) * WORDS_IN_SEGMENT;
const int64_t WORDS_IN_CHUNK = (CHUNK_SIZE / SEGMENT_LEN) * WORDS_IN_SEGMENT;
const int64_t WORDS_IN_SLICE = (SLICE_SIZE / SEGMENT_LEN) * WORDS_IN_SEGMENT;
const int64_t CHUNKS_IN_SLICE = WORDS_IN_SLICE / WORDS_IN_CHUNK;

const uint8_t OFFSET[] = {
  0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 3, 3, 3, 4, 4, 5, 5, 5, 5, 6, 6, 6,
  6, 6, 6, 7, 7, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 10, 10, 11, 11, 11, 11, 12, 12,
  12, 12, 12, 12, 13, 13, 13, 13, 13, 13, 14, 14, 15, 15, 15, 15, 15, 15, 16,
  16, 16, 16, 17, 17, 18, 18, 18, 18, 18, 18, 19, 19, 19, 19, 20, 20, 20, 20,
  20, 20, 21, 21, 21, 21, 21, 21, 21, 21, 22, 22, 22, 22, 23, 23, 24, 24, 24,
  24, 25, 25, 26, 26, 26, 26, 27, 27, 27, 27, 27, 27, 27, 27, 28, 28, 28, 28,
  28, 28, 29, 29, 29, 29, 30, 30, 30, 30, 30, 30, 31, 31, 32, 32, 32, 32, 33,
  33, 33, 33, 33, 33, 34, 34, 35, 35, 35, 35, 35, 35, 36, 36, 36, 36, 36, 36,
  37, 37, 37, 37, 38, 38, 39, 39, 39, 39, 40, 40, 40, 40, 40, 40, 41, 41, 42,
  42, 42, 42, 42, 42, 43, 43, 43, 43, 44, 44, 45, 45, 45, 45, 46, 46, 47, 47,
  47, 47, 47, 47, 47, 47, 47, 47, 48
};

const int64_t ROFFSET[] = {
  1, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83,
  89, 97, 101, 103, 107, 109, 113, 121, 127, 131, 137, 139, 143, 149, 151, 157,
  163, 167, 169, 173, 179, 181, 187, 191, 193, 197, 199, 209, 211, 221, 223, 227,
  229, 233, 239, 241, 247, 251, 253, 257, 263, 269, 271, 277, 281, 283, 289, 293,
  299, 307, 311, 313, 317, 319, 323, 331, 337, 341, 347, 349, 353, 359, 361, 367,
  373, 377, 379, 383, 389, 391, 397, 401, 403, 407, 409, 419, 421, 431, 433, 437,
  439, 443, 449, 451, 457, 461, 463, 467, 473, 479, 481, 487, 491, 493, 499, 503,
  509, 517, 521, 523, 527, 529, 533, 541, 547, 551, 557, 559, 563, 569, 571, 577,
  583, 587, 589, 593, 599, 601, 607, 611, 613, 617, 619, 629, 631, 641, 643, 647,
  649, 653, 659, 661, 667, 671, 673, 677, 683, 689, 691, 697, 701, 703, 709, 713,
  719, 727, 731, 733, 737, 739, 743, 751, 757, 761, 767, 769, 773, 779, 781, 787,
  793, 797, 799, 803, 809, 811, 817, 821, 823, 827, 829, 839
};

const bool SEMIPRIME357[] = {
  0, 1, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0,
  1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0,
  1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0,
  0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1,
  1
};

#define INDEX(n) ((((n) - 1) / 210) * OFFSET[209] + OFFSET[((n) - 1) % 210])
#define RINDEX(k, l) ((k / WORDS_IN_SEGMENT) * SEGMENT_LEN + ROFFSET[(k * 64) % VALS_IN_SEGMENT + l])
