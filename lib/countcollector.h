#include "binaryfile.h"
#include "common.h"
#include "constants.h"

#include <atomic>
#include <cassert>
#include <cstring>
#include <cstdlib>
#include <memory>
#include <queue>
#include <tuple>
#include <vector>

class CountCollector {
 private:
  const std::vector<int32_t>& primes_;
  const bool s2;
  std::vector<int64_t> int2357_[2];
  int32_t* counts;

  static int64_t totalSlices_, range_, totalSamples_;
  static int32_t* samples_[NUMBER_OF_THREADS];

 public:
  static void init(int64_t totalSlices) {
    assert(totalSlices_ == 0);
    assert(samples_[0] == nullptr);
    assert(totalSlices % 2 == 0);

    totalSlices_ = totalSlices;
    range_ = totalSlices * SLICE_SIZE;
    totalSamples_ = totalSlices * SLICE_SIZE / SAMPLE_INTERVAL;

    for (int64_t t = 0; t < NUMBER_OF_THREADS; t++) {
      samples_[t] = new int32_t[totalSamples_];
    }
  };

  static void destruct() {
    for (int64_t t = 0; t < NUMBER_OF_THREADS; t++) {
      delete[] samples_[t];
    }
  }

  CountCollector(
      const std::vector<int32_t>& primes,
      bool s2,
      bool s3,
      bool s5,
      bool s7) : primes_(primes), s2(s2) {

    assert(samples_[0] != nullptr);
    counts = new int32_t[totalSamples_];
    std::memset(counts, 0, totalSamples_ * (sizeof(int32_t) / sizeof(char)));
    generate2357(s2, s3, s5, s7);
  }

  ~CountCollector() {
    delete[] counts;
  }

  void collectCounts(int sliceId, const int64_t* slice) {
    assert(WORDS_IN_SAMPLE_INTERVAL % 2 == 0);

    Stopwatch stopwatch("Collecting counts from slice " + toString(sliceId));
    for (int64_t t = 0; t < NUMBER_OF_THREADS; t++) {
      std::memset(
          samples_[t], 0, totalSamples_ * (sizeof(int32_t) / sizeof(char)));
    }

    int64_t sliceStart = SLICE_SIZE * sliceId + 1;
    if (sliceStart * 2 > range_) {
      collectCountsHigh(sliceId, slice);
    } else if (sliceStart * 3 > range_) {
      collectCountsMid(sliceId, slice);
    } else {
      collectCountsLow(sliceId, slice);
    }
  }

  void storeCounts(std::shared_ptr<BinaryFile> file) {
    file->write((char*) &counts[0], totalSamples_ * 4);
  }

 private:
  void collectCountsLow(int sliceId, const int64_t* slice) {
    int64_t sliceStart = SLICE_SIZE * sliceId + 1;
    std::atomic<int64_t> nextIntStart(0), nextThreadId(0);
    auto worker = [&]() {
      int64_t threadId = nextThreadId.fetch_add(1);
      int32_t* samples = samples_[threadId];

      while (true) {
        int64_t chunkStart = nextIntStart.fetch_add(WORDS_IN_CHUNK);
        if (chunkStart >= WORDS_IN_SLICE) {
          break;
        }

        for (int64_t intStart = chunkStart;
             intStart < chunkStart + WORDS_IN_CHUNK;
             intStart += WORDS_IN_SAMPLE_INTERVAL) {

          int32_t sum = 0;
          for (int64_t wordIdx = intStart;
               wordIdx < intStart + WORDS_IN_SAMPLE_INTERVAL;
               wordIdx++) {

            sum += __builtin_popcountll(slice[wordIdx]);
            for (int s = 0; s < 64; s++) {
              int64_t n = sliceStart + RINDEX(wordIdx, s) - 1;
              bool nval = (slice[wordIdx] & (((uint64_t) 1) << s));
              for (int64_t d : int2357_[nval]) {
                int64_t nn = n * d;
                if (nn > range_) break;
                int64_t idx = (nn - 1) / SAMPLE_INTERVAL;
                samples[idx]++;
              }
            }
          }
          counts[(WORDS_IN_SLICE * sliceId + intStart) / WORDS_IN_SAMPLE_INTERVAL] += sum;
        }
      }
    };
    runMultiThreaded(worker);
    addSamplesToCounts();
  }

  void collectCountsMid(int sliceId, const int64_t* slice) {
    int64_t sliceStart = SLICE_SIZE * sliceId + 1;
    int64_t sliceEnd = sliceStart + SLICE_SIZE - 1;
    assert(sliceStart * 2 > sliceEnd);
    assert(sliceEnd * 2 <= range_);

    std::atomic<int64_t> nextIntStart(0);
    auto worker = [&]() {
      while (true) {
        int64_t chunkStart = nextIntStart.fetch_add(WORDS_IN_CHUNK);
        if (chunkStart >= WORDS_IN_SLICE) {
          break;
        }

        for (int64_t intStart = chunkStart;
             intStart < chunkStart + WORDS_IN_CHUNK;
             intStart += WORDS_IN_SAMPLE_INTERVAL) {

          int32_t sum = 0;
          for (int64_t wordIdx = intStart;
               wordIdx < intStart + WORDS_IN_SAMPLE_INTERVAL / 2;
               wordIdx++) {

            sum += __builtin_popcountll(slice[wordIdx]);
          }

          int32_t sum2 = 0;
          for (int64_t wordIdx = intStart + WORDS_IN_SAMPLE_INTERVAL / 2;
               wordIdx < intStart + WORDS_IN_SAMPLE_INTERVAL;
               wordIdx++) {

            sum2 += __builtin_popcountll(slice[wordIdx]);
          }

          int64_t sampleIdx =
            (WORDS_IN_SLICE * sliceId + intStart) / WORDS_IN_SAMPLE_INTERVAL;
          counts[sampleIdx] += sum + sum2;
          counts[sampleIdx * 2] += s2 ? sum : WORDS_IN_SAMPLE_INTERVAL * 32 - sum;
          counts[sampleIdx * 2 + 1] += s2 ? sum2 : WORDS_IN_SAMPLE_INTERVAL * 32 - sum2;
        }
      }
    };
    runMultiThreaded(worker);
  }

  void collectCountsHigh(int sliceId, const int64_t* slice) {
    std::atomic<int64_t> nextIntStart(0);
    auto worker = [&]() {
      while (true) {
        int64_t chunkStart = nextIntStart.fetch_add(WORDS_IN_CHUNK);
        if (chunkStart >= WORDS_IN_SLICE) {
          break;
        }

        for (int64_t intStart = chunkStart;
             intStart < chunkStart + WORDS_IN_CHUNK;
             intStart += WORDS_IN_SAMPLE_INTERVAL) {

          int32_t sum = 0;
          for (int64_t wordIdx = intStart;
               wordIdx < intStart + WORDS_IN_SAMPLE_INTERVAL;
               wordIdx++) {

            sum += __builtin_popcountll(slice[wordIdx]);
          }
          counts[(WORDS_IN_SLICE * sliceId + intStart) / WORDS_IN_SAMPLE_INTERVAL] += sum;
        }
      }
    };
    runMultiThreaded(worker);
  }

  void addSamplesToCounts() {
    std::atomic<int64_t> nextSample(0);
    const int64_t samplesPerRun = 10000000;
    auto worker = [&]() {
      while (true) {
        int64_t start = nextSample.fetch_add(samplesPerRun);
        if (start >= totalSamples_) {
          break;
        }

        int64_t end = std::min(start + samplesPerRun - 1, totalSamples_ - 1);
        for (int64_t i = start; i <= end; i++) {
          for (int64_t t = 0; t < NUMBER_OF_THREADS; t++) {
            counts[i] += samples_[t][i];
          }
        }
      }
    };
    runMultiThreaded(worker);
  }

  void generate2357(bool s2, bool s3, bool s5, bool s7) {
    typedef std::tuple<int64_t, int> Elem;
    std::priority_queue<Elem, std::vector<Elem>, std::greater<Elem>> Q;
    const std::vector<int64_t> P{2, 3, 5, 7};
    const std::vector<int64_t> A{!s2, !s3, !s5, !s7};

    for (int i = 0; i < 4; i++) {
      Q.push(std::make_tuple(P[i], A[i]));
    }

    while (true) {
      Elem e = Q.top();
      Q.pop();
      auto n = std::get<0>(e);
      auto cnt = std::get<1>(e);

      if (n > range_) {
        break;
      }
      int2357_[1 - cnt % 2].push_back(n);

      for (int i = 3; i >= 0; i--) {
        if (n % P[i] == 0) {
          for (int j = i; j < 4; j++) {
            Q.push(std::make_tuple(P[j] * n, cnt + A[j]));
          }
          break;
        }
      }
    }
  }
};

int32_t* CountCollector::samples_[NUMBER_OF_THREADS];
int64_t CountCollector::totalSlices_;
int64_t CountCollector::range_;
int64_t CountCollector::totalSamples_;
