#include "lib/common.h"
#include "lib/constants.h"
#include "lib/sliceloader.h"
#include "lib/slicecomputation.h"

using namespace std;

void checkPreconditions() {
  assert(SAMPLE_INTERVAL % SEGMENT_LEN == 0);
  assert(CHUNK_SIZE % SAMPLE_INTERVAL == 0);
  assert(SLICE_SIZE % CHUNK_SIZE == 0);
  assert(VALS_IN_SEGMENT % 64 == 0);
  for (int64_t k = 0; k < 64; k++) {
    auto start = k * SAMPLE_INTERVAL + 1;
    auto end = start + SAMPLE_INTERVAL - 1;
    assert(INDEX(start) % 64 == 0);
    assert(INDEX(end - 1) % 64 == 63);
  }
}

int main(int argc, const char* argv[]) {
  checkPreconditions();

  if (argc != 4) {
    printf("Usage: %s start_slice end_slice slices_dir\n", argv[0]);
    return 1;
  }

  int start = atoi(argv[1]);
  int end = atoi(argv[2]);
  assert(0 <= start);
  assert(start <= end);

  string dir = argv[3];
  auto sliceLoader = make_shared<SliceLoader>(dir);
  auto primes = make_shared<Primes>((end + 1) * SLICE_SIZE);

  int maxSliceNecessary = -1;
  map<int, shared_ptr<SliceComputation>> sliceCalculators;
  for (int newSliceId = start; newSliceId <= end; newSliceId++) {
    assert(!sliceLoader->sliceExists(newSliceId));

    auto calculator = make_shared<SliceComputation>(
        sliceLoader->allocSlice(newSliceId),
        newSliceId,
        primes->getPrimes());

    sliceCalculators[newSliceId] = calculator;
    maxSliceNecessary =
      max(maxSliceNecessary, calculator->getMaxSliceNecessary());
  }

  if (maxSliceNecessary > sliceLoader->getMaxSliceAvailable()) {
    printf(
        "All slice up to %d are necessary, but max available is %d.\n",
        maxSliceNecessary,
        sliceLoader->getMaxSliceAvailable());
    abort();
  }

  for (int sliceId = 0; sliceId <= maxSliceNecessary; sliceId++) {
    int64_t* slice = sliceLoader->loadSlice(sliceId);
    for (auto calculator : sliceCalculators) {
      calculator.second->processSlice(sliceId, slice);
    }
  }

  sliceLoader->storeSlicesOnDisk();
  return 0;
}
