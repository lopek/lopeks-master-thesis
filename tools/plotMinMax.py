import sys, math, matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({'font.size': 9})

if len(sys.argv) not in [4, 5]:
    print('Usage: %s <lvalues_file> <output_plot_file> <log|lin> <opt: c>' % sys.argv[0])
    sys.exit(1)

if sys.argv[3] not in ['log', 'lin']:
    print('Invalid plot type: %s. Options: `log` or `lin`' % sys.argv[3])
    sys.exit(1)

logscale = (sys.argv[3] == 'log')

C = 0
if len(sys.argv) == 5:
    C = float(sys.argv[4])

raw_lvalues_file = open(sys.argv[1])
X, minY, maxY = [], [], []

for l in raw_lvalues_file:
    if l.startswith('#'):
        continue
    
    try:
        x, miny, maxy = map(int, l.strip().split())
    except:
        print('Failed to parse line: %s' % l);
        sys.exit(2)

    X.append(x)
    minY.append(miny)
    maxY.append(maxy)

print('Parsed %d (x, miny, maxy) tuples. Constructing plot...' % len(X))

fig = plt.figure()
splt = fig.add_subplot(111)
if max(maxY) > 0:
    splt.axhline(y=0, color='#EEEEEE')

color = '0.75' if C else None

if logscale:
    splt.semilogx(X, minY, 'b', color=color)
    splt.semilogx(X, maxY, 'r', color=color)
    splt.grid(True)
else:
    splt.plot(X, minY, 'b', color=color)
    splt.plot(X, maxY, 'r', color=color)

if C and logscale:
    splt.semilogx(X, map(lambda x : C * math.sqrt(x), X), 'k--')
elif C:
    splt.plot(X, map(lambda x : C * math.sqrt(x), X), 'k--')

for k in range(6, 16):
    if 10 ** k <= X[-1] and X[-1] <= 10 ** k + 10 ** (k - 1):
        plt.gca().set_xlim([0, 10 ** k])

print('Saving plot to %s...' % sys.argv[2])
plt.savefig(sys.argv[2], dpi=900, bbox_inches='tight')
